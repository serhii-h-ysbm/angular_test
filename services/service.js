var iiModule = angular.module('iiModule', []);

iiModule.controller('IiController', ['$scope', 'ii', function($scope, ii) {
    $scope.iiSays = function(isSlow) {
        ii.answer(isSlow);
    }
}]);

iiModule.factory('ii', ['$timeout', function($timeout) {
    return {
        answer: function (isSlow) {
            if(isSlow){
                this.slow();
            }else{
                this.fast();
            }
        },
        fast: function() {
            alert ('Hello world, my friend.');
        },
        slow: function() {
            $timeout(this.fast, 3000);
        }
    };
}]);